### Road Map - 2014-09-13

This release represents a major refactoring and change of philosophy for CompJournoStick. As a result, I am calling it v3.0.0. Major changes:

1. I have abandoned the thoughts / delusions of shipping CRAN task views. They take too long to download and install and contain way more software than I can hope to explore. They also require numerous header files (Fedora "-devel" packages). I have also stopped trying to collect R packages that I think will be of some use to journalists. However, the end-user ***Fedora*** package bill of materials is essentially the same as the previous releases.
1. I have factored out the installation of most of the R packages into an R package called "compjourninstaller". This means that the R packages in CompJournoStick and [CompJournWin8Pro](https://gitlab.com/znmeb/CompJournWin8Pro) are nearly identical and I only have to maintain one package. "compjourninstaller" can be found on ***Bitbucket*** at <https://bitbucket.org/znmeb/compjourninstaller>.
1. I have added some web application stacks:
    * Python: Django and Flask
    * Ruby: Rails and Sinatra
    * Node.js: Express
1. I dropped Riak - no R client library. And I dropped Hadoop. Hadoop will probably return during Fedora 21 alpha testing.
1. I have added a repository for the latest QGIS (Quantum GIS). It has some features not available in the version of QGIS that ships with Fedora 20.
1. All of the open source tools and packages from RStudio are now in CompJournoStick except RStudio server and Shiny Server. That includes the latest version of R Markdown and the package management tool Packrat. 

Future plans:

1. I have started alpha testing on Fedora 21. I expect to release CompJournoStick 21 concurrently with the Fedora 21 release, sometime in November.
1. I will be tracking RStudio more or less daily and any enhancements will show up in CompJournoStick as soon as I can get them in.
1. I am exploring static site generators. There are two available in Fedora 20, Hakyll, a Jekyll-like package written in Haskell, and Nikola, written in Python. I will consider others, however.

### Road Map - 2014-04-28

This release (v2.3.0) has a number of minor enhancements:

1. More LaTex tools to support the updated RStudio
1. More gdal scripting language support
1. More shiny-based R packages

Future plans:

1. As I've noted elsewhere, I'm migrating projects from GitHub to Gitlab. I don't have a target date for migrating CompJournoStick, though. 
1. After looking at what's involved in updating [Computational Journalism Publishers Workbench](https://github.com/znmeb/Computational-Journalism-Publishers-Workbench) to Ubuntu 14.04 LTS, I decided to abandon that project. The Fedora version is superseded by CompJournoStick, and I'm replacing the Ubuntu version with a new project, [OSGeo Live AddOns](https://gitlab.com/znmeb/osgeo-live-addons), which uses [OSGeo Live](http://live.osgeo.org/en/index.html) as its base. The code is working, but I need to completely revamp the documentation.

### Road Map - 2014-03-24

This release (v2.2.0) has a single major enhancement: [RStudio Preview Release 0.98b](http://www.rstudio.com/ide/download/preview). See the release notes for more details.

Future plans:

1. Ubuntu 14.04LTS is scheduled to ship on April 17, 2014. Once it ships I will be updating the [Computational Journalism Publishers Workbench](http://znmeb.github.io/Computational-Journalism-Publishers-Workbench/) to work with it. I'm guessing that will take at least a week, and I'll be normalizing the Linux and R packages to match the current release of ___CompJournoStick___. So look for it in early May.
1. I'm starting a "publication" of tutorials and stories created using ___CompJournoStick___. The working title is "Reproducible Journalism Weekly". As the name implies, I'm planning to publish weekly, with perhaps some larger pieces every month. I don't have the format entirely designed yet, but I'm hoping it will be "responsive" enough to work on a small screen, for example, a ZTE Open Firefox OS phone. "Reproducible Journalism Weekly" will be subscription-only; the tentative pricing is "pay what you want" with a minimum of $3.99 a month.

### Road Map - 2014-02-23

I'm targeting this release (v2.1.0) for [NICAR 2014](http://ire.org/conferences/nicar-2014/). There are quite a few new Linux and R packages since v2.0.0.

1. Rails, Django and Flask are now included.
1. I've added Solr, Lucene and Tika.
1. There are more R data collection, sports, weather, time series and finance packages.
1. I've added the new rMaps and dplyr R packages.
1. I've added some math packages, including Octave and the Python symbolic math package SymPy.
1. There are now four ISO file creation options:

    * Create an ISO with the same architecture as the system where the script is running,
    * Create a 32-bit ISO,
    * Create a 64-bit ISO, or
    * Create both 32-bit and 64-bit ISOs.

Future plans:

1. The crowdfunding launch will be after NICAR 2014. I want to get some feedback from users before I push forward with the project. I still don't know whether I'll launch on Kickstarter, IndieGoGo or Ramen, but IndieGoGo is the front-runner at this time.
1. There's no Fedora 21 release schedule yet, but the release will not be before August 2014. The main additions I'm waiting for in F21 are the Apache tools built on top of Hadoop. Hadoop itself is in ___CompJournoStick___ as of the current v2.1.0 release, but HBase and some of the other packages are in process.
1. More Python: I'm not a Python programmer, so I don't know what's available that's relevant for computational journalism. I currently have everything I know about, and when I see cool Python-based stories, I'll be adding the tools to make them.
1. My Census, email mining and Firefox OS tools will be moved into ___CompJournoStick___ when they're solid enough.

### Road Map - 2013-12-08

The current (v1.9.0) release features

1. The biggest change is that the R packages are now installed in the main R library as 'root', both on a bare metal/virtual machine install via the scripts and on the ISO file! That means getting up and running is a much simpler process:
    * Install Fedora - any desktop - on bare metal or in a virtual machine.
    * Download the scripts.
    * Change into the 'CompJournoStick/1SetUpWorkstation' directory and type `sudo ./1<tab><enter>`.
1. I've added the [IPython](http://ipython.org/index.html) and [IPython Notebook](http://ipython.org/notebook.html) tools.
1. I've added the NoSQL databases CouchDB, Riak, MongoDB and Redis.
1. Hosting support for virtual machines:
    * VMware Workstation 10 and VMware Player 6 hosts are supported. ***Older VMware versions are no longer supported; Fedora compatibility is rocky at best on them.***
    * VirtualBox 4.3 hosts are now supported.
    * Virtual Machine Manager 0.10.0 on a Fedora 20 host is now supported.
1. Both Fedora 19 and Fedora 20 beta are supported. I've been running my laptops and workstation on Fedora 20 since the alpha release and there have only been a few minor glitches.
1. I had to delete all the extra desktops from the ISO build process; there were too many issues with video drivers that were dominating my testing time. So now the main desktop is the Fedora default GNOME 3. I've added an extremely lightweight desktop environment, [Openbox](http://openbox.org/) with [fbpanel](http://fbpanel.sourceforge.net/), and I've changed the display manager to [lightdm](http://www.freedesktop.org/wiki/Software/LightDM/). 

Future plans:

1. My next big project is an email analysis toolkit. I don't know if this will be part of ___CompJournoStick___ or not, mostly because I haven't assessed the RAM / CPU requirements yet. The algorithms are well-documented in the literature; see for example [http://www.r-project.org/conferences/useR-2009/slides/Bohn+Feinerer+Hornik+Theussl.pdf](http://www.r-project.org/conferences/useR-2009/slides/Bohn+Feinerer+Hornik+Theussl.pdf), but I have no idea how they scale.
1. Because of the massive amount of change from previous releases and because Fedora 20 is still in beta, I consider this a preview release and I'm calling it v1.9.0. There may be point releases before Fedora 20 is released, and there will definitely be a v2.0.0 some time shortly after the Fedora 20 release.
1. Kickstarter or Ramen? I had planned to do a Kickstarter project for creating media - DVDs and USB sticks - but there's a new possibility: [Ramen](https://twitter.com/ramenapp). See the [Ramen project site](http://www.ramen.is/projects/ramen) for more details. 

### Road Map - 2013-10-14

The current release features

1. *All* of the Linux packages from the [Computational Journalism Publishers Workbench](http://znmeb.github.io/Computational-Journalism-Publishers-Workbench) should now be on the build. This includes
    * the PostgreSQL relational database management system,
    * all of the geospatial (GIS) packages,
    * all of the web scraping and Twitter utilities,
    * the Python Natural Language Toolkit (NLTK), and
    * the Python NetworkX social network analysis package.
1. Node.js and npm are now installed. Node.js was coming in as a dependency from some of the spatial packages, and npm is widely used by web developers.
1. Mind mapping tools [vym](http://www.insilmaril.de/vym/), [freemind](http://freemind.sourceforge.net/wiki/index.php/Main_Page), [labyrinth](https://github.com/labyrinth-team/labyrinth) and [semantik](http://code.google.com/p/semantik/) are now installed.
1. Static site generator [Hakyll](http://jaspervdj.be/hakyll/) is now installed.
1. The [Mnemosyne](http://mnemosyne-proj.org/) learning system is now installed.
1. I have done much refactoring of the scripts:
    * The [ISO creation script](https://github.com/znmeb/CompJournoStick/blob/master/2MakeLiveISOImage/1SudoMakeISOImage.bash) no longer has parameters. There is only one desktop (GNOME 3 + GNOME Classic Session) and both x86_64 and i686 ISO files are built by default.
    * The individual components of the [tool installation script](https://github.com/znmeb/CompJournoStick/blob/master/1SetUpWorkstation/1SudoInstallTools.bash) have been made available for maintainers to use on an installed system.

Future plans:

1. The next step is a complete refactoring of the documentation, with a "Mobile First" strategy. Specifically, I am planning to deliver it as a Firefox OS application which will work on a Firefox phone, Firefox for Android or the stable Firefox desktop browser. It will probably also work with Chrome and may work with the browser in iOS devices, but I have no plans to test on either.
1. I have the bill of materials where I want it for release of packaged appliances. Once I get the documentation done, I will start the Kickstarter campaign to deliver ISO files, Open Virtual Format virtual machines, DVDs and bootable USB sticks. I would like it to be ready for Black Friday but that will be a stretch.
1. The code is working now on Fedora 20 bare metal installs, but I am still seeing dependency issues in the ISO build process.

### Road Map - 2013-09-22

The current release features

1. *All* of the Linux packages from the [Computational Journalism Publishers Workbench](http://znmeb.github.io/Computational-Journalism-Publishers-Workbench) should now be on the build. This includes
    * the PostgreSQL relational database management system,
    * all of the geospatial (GIS) packages,
    * all of the web scraping and Twitter utilities,
    * the Python Natural Language Toolkit (NLTK), and
    * the Python NetworkX social network analysis package.
1. There's no longer a 'desktop' choice when building a virtual machine or an ISO. You get all six Fedora-supported desktops
    * GNOME 3 (default)
    * KDE Plasma Workspace
    * Cinnamon
    * MATE
    * XFCE
    * LXDE

There isn't going to be much happening this week on this project, and there won't be another feature release until October. Bug fixes will happen, though - this release is quite ambitious and I expect there are a few loose ends.

### Road Map - 2013-09-15

The current release features

1. I've added [LibreOffice](http://www.libreoffice.org/) to the Linux packages.
2. I've added the PDF editing and data extraction tools from the [Computational Journalism Publishers Workbench](http://znmeb.github.io/Computational-Journalism-Publishers-Workbench/) to the Linux packages.
3. I've added the Linux packages from the [Fedora Design Suite](http://spins.fedoraproject.org/design/).

I've done some testing with Fedora 20 and it's in pretty good shape. I only ran into one minor issue - a few non-essential packages aren't in the repositories. So I expect the transition from Fedora 19 to Fedora 20 to be painless.

### Road Map - 2013-09-09

The current release is just a resting place / bug fix release. I've added some R
packages that I'm using for [pdxcensusdata](http://znmeb.github.io), which are
mostly Census data or packaging tools.

There isn't going to be much happening here till after [WhereCampPDX 6](http://wherecamppdx.org/). I will most likely freeze the [Computational Journalism Publishers Workbench](http://znmeb.github.io) after pushing any bug fixes to it between now and WhereCamp. 

Then I'm planning to fork the Ubuntu part of the workbench to a new project, a computational journalism add-on package built on top of [OSGeo-Live 7.0](http://live.osgeo.org/en/index.html). This will be delivered as an R package and may not include RStudio.

[Fedora 20 is coming!](http://news.softpedia.com/news/Fedora-20-Release-Schedule-381126.shtml) I'll be alpha-testing ___CompJournoStick___ on bare metal the moment an ISO is available.

### Road Map - 2013-09-01

The current release features

1. Most of the Markdown documents have been replaced by slideshows, built with [Slidify](http://ramnathv.github.io/slidify/).
1. I've added the Linux dependencies for the 'Spatial' task view, though not the task view itself.
1. I've added the 'USCensus2010' and 'acs' R packages.
1. RStudio Desktop is now the [latest ***preview*** release](http://www.rstudio.com/ide/download/preview).
1. ISO files are always 'de-branded' now.
1. I've added a number of Fedora packages for secure communications. They are
    * [tor and vidalia](https://www.torproject.org/)
    * [bitlbee/bitlbee-otr](http://www.bitlbee.org/main.php/news.r.html)
    * [irssi/irssi-otr](http://www.irssi.org/)
    * [pidgin/pidgin-otr](http://www.pidgin.im/)
    * [xchat/xchat-otr](http://xchat.org/)
    * [thunderbird/thunderbird-enigmail](https://www.enigmail.net/home/index.php)
    * [claws-mail/claws-mail-plugins-pgp](http://www.claws-mail.org/index.php)

The next steps are:

1. I'm going to package some tools for working with Census data for the Portland-Vancouver-Salem area. The repository is [https://github.com/znmeb/PDX-Census-Code](https://github.com/znmeb/PDX-Census-Code). This should be completed in time for [WhereCampPDX 6](http://wherecamppdx.org/).
1. Staying on the spatial theme, I'm working my way through [_Applied Spatial Data Analysis with R, Second Edition_](http://j.mp/15tVLvX). As noted above, all the Linux dependencies should be in ___CompJournoStick___ but the R packages will be added as needed.
1. I'm planning to devise scripts for making encrypted USB sticks. I have some preliminary versions but want to do some more testing before I release them. These will require at least a 16 GB stick, and larger is better. I regularly test on a 32 GB USB 3.0 stick.
1. More slide shows - I especially need to document the usage of the encrypted communications tools.

### Road Map - 2013-08-17

The current release features a major update to the ISO creation script. It now allows creation of either generic or Fedora-branded ISOs, choice of five desktops and the ability to select either i686 (32-bit) or x86_64 (64-bit) software. I've fixed some bugs and added some more editing tools, but the expanded ISO creation is the main feature of the release.

The next steps are:

1. Continue testing the non-GNOME desktops. I've found a few bugs but I'm sure there are more.
1. Review Jonathan Stray's [Computational Journalism Lectures](http://jmsc.hku.hk/courses/jmsc6041spring2013/syllabus/) and see what's involved in coding some of the exercises in R/RStudio, with a "reproducible research" discipline. I don't have a good estimate of the effort involved yet, but it's something I've wanted to do since he posted the lectures.
1. Start porting the documents to a more "responsive" web site. Right now, they're pretty minimal and just take advantage of Github's automatic rendering of Markdown files. Given the tools in ___CompJournoStick___ and the full flexibility of Github Pages I know it's possible to do much better.

### Road Map - 2013-08-11

The current release mostly features bug fixes and the addition of screenshots in the document files. They're stored on Flickr, and you have to click over to Flickr with the links to view them. This is a temporary situation; the plan is to have self-contained HTML documents on Github Pages. I'm sticking with VMware Player as the hosting mechanism for this round. 

I am working my way through [_Reproducible Research with R and RStudio_](http://j.mp/RepRsch) by [Christopher Gandrud](https://github.com/christophergandrud). While the book is aimed at researchers, it contains an excellent introduction to R and RStudio and has a complete workflow for data projects ranging from simple web pages and slide presentations to complete textbooks. The book itself was built using the workflow and a draft version is available as a [Github repository](https://github.com/christophergandrud/Rep-Res-Book)! 

I will be incorporating as much of this workflow as I can into the ___CompJournoStick___ tool set. I think this release has everything a user needs to implement Gandrud's workflow, but if I find anything missing I'll be updating the tool. The goal is a complete environment for doing reproducible computational journalism.

I'm still planning a Kickstarter project to take ___CompJournoStick___ to the next level. I'm still trying to get the project scope and the budget pared down to a "minimum viable product"; I think what I have now is still too big. In particular, I want a lighter weight desktop than GNOME 3 and I don't see the need for Evolution or LibreOffice. Personally, I don't use them and they take up a lot of space on the USB stick that would be better used for data.

### Road Map - 2013-08-05

Now that release 1.0.0 is out, I'm going to focus on finishing the documentation for installing to bare metal, building ISO files and installing them to USB sticks. The scripts work, but there are enough gotchas lurking about for the beginner that I'd prefer users stick with running ___CompJournoStick___ in virtual machines for now.

I am planning a Kickstarter campaign to come up with actual branded USB sticks that one can boot. The money will be used to hire a designer for the branding, test on Macintosh laptops and set up a manufacturing / fulfillment process. As the saying goes, 'Watch this space!'
