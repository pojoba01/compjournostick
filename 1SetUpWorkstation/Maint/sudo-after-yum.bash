#! /bin/bash
#
# Copyright (C) 2013 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

# make sure we can see R and other stuff
export PATH=/usr/local/bin:$PATH

# make sure fbpanel starts with openbox
echo 'fbpanel &' >> /etc/xdg/openbox/autostart

# force GDM display manager
system-switch-displaymanager gdm

# Set command line link to RStudio Desktop
ln -sf /usr/lib/rstudio/bin/rstudio /usr/local/bin/rstudio

# hide RStudio's 'pandoc' - Fedora's works!
mv /usr/lib/rstudio/bin/pandoc /usr/lib/rstudio/bin/pandoc.bak

# set site-wide R profile
cp Rprofile.site `R RHOME`/etc/Rprofile.site

# initialize R Java interface
R CMD javareconf

# set symbol for R WordNet code
Maint/set-wordnet.bash

# set aliases
cp CompJournoStick.sh /etc/profile.d

# install 'base':
# RStudio dependencies
# R Commander and task views
R --no-save --no-restore < R-scripts/install-base.R

# NoSQL client library packages
R --no-save --no-restore < R-scripts/install-nosql.R

# set up R Commander desktop start!
mkdir -p /usr/local/share/applications
cp `R RHOME`/library/Rcmdr/etc/linux/Rcmdr.desktop \
  /usr/local/share/applications
mkdir -p /usr/lib/R/site-library/Rcmdr/etc/linux/
cp `R RHOME`/library/Rcmdr/etc/linux/Rcmdr.svg \
  /usr/lib/R/site-library/Rcmdr/etc/linux/
