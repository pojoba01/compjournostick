#! /bin/bash
#
# Copyright (C) 2013 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

# needed for later
yum install -y patch yum-plugin-fastestmirror wget

# lower the maximum number of connections and raise minimum rate
patch -N -b /etc/yum.conf yum.conf.patch 

# get rid of distro-supplied VMware tools
Maint/sudo-erase-open-vm-tools.bash

# cleanup and update Fedora packages
Maint/sudo-update-fedora-packages.bash

# install Fedora packages
Maint/sudo-install-fedora-packages.bash

# RStudio Desktop
Maint/sudo-update-rstudio.bash 2>&1 | tee update-rstudio.log

# source installs from upstream, etc.
Maint/sudo-after-yum.bash 2>&1 | tee after-yum.log
Maint/check-r-packages.bash after-yum.log

# clock
system-config-date

# de-root any created files
chown -R $UID.$UID $HOME
