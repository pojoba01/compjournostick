#! /bin/bash
#
# Copyright (C) 2012 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

mkdir -p /mnt/cdrom
mount /dev/sr0 /mnt/cdrom
tar xf /mnt/cdrom/VMwareTools-*.tar.gz
cd vmware-tools-distrib
./vmware-install.pl -d
cd ..
rm -fr vmware-tools-distrib # cleanup!!!
umount /dev/sr0
