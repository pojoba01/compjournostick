#! /bin/bash
#
# Copyright (C) 2013 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

# figure out version
VERSION=\
`yum version nogroups|grep Installed|sed 's;Installed: ;;'|sed 's;/.*$;;'`
echo "Fedora installed release version is ${VERSION}"

# create a tmpdir
export HERE=`pwd`
rm -fr ${HERE}/tmpdir
mkdir -p ${HERE}/tmpdir

# architecture list
export ARCHLIST=${1}

# save project for live image
pushd ../..
rm -fr /opt/Project
cp -a CompJournoStick* /opt/Project
rm -fr /opt/Project/.git*
rm -fr /opt/Project/.R*
rm -fr /opt/Project/2MakeLiveISOImage/tmpdir
popd

# build the kickstart file
cat top.ks > CompJournoStick.ks
cat ../1SetUpWorkstation/packagelist.txt >> CompJournoStick.ks
cat deletions.ks >> CompJournoStick.ks
cat debranding.ks >> CompJournoStick.ks
cat bottom.ks >> CompJournoStick.ks

# set up place where we'll build the ISO
mkdir -p /opt/ISOs/ # for saved ISOs
rm -fr /opt/CompJournoStick/; mkdir -p /opt/CompJournoStick/
cp /usr/share/spin-kickstarts/*.ks /opt/CompJournoStick/ # base kickstart files
mv CompJournoStick.ks /opt/CompJournoStick/ # our over-rides

pushd /opt/CompJournoStick/
setenforce 0 # disable SELinux

# make the ISOs
for ARCH in $ARCHLIST
do
  setarch ${ARCH} \
  /usr/bin/time livecd-creator \
    --config=CompJournoStick.ks \
    --releasever=${VERSION} \
    --fslabel=CompJournoStick${VERSION}-${ARCH} \
    --title=CompJournoStick${VERSION}-${ARCH} \
    --product=CompJournoStick${VERSION}-${ARCH} \
    --cache=/var/cache/live \
    --tmpdir=${HERE}/tmpdir \
    2>&1 | tee ${VERSION}-${ARCH}.log

  # check the ISO
  checkisomd5 CompJournoStick${VERSION}-${ARCH}.iso 2>&1 | tee ${VERSION}-${ARCH}.check
  ${HERE}/../1SetUpWorkstation/Maint/check-r-packages.bash \
    ${VERSION}-${ARCH}.log
  mv CompJournoStick${VERSION}-${ARCH}.iso /opt/ISOs/ # save for later
  mv ${VERSION}-${ARCH}.log /opt/ISOs/
done

setenforce 1 # enable SELinux
popd
