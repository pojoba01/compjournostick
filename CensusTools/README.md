## Quick guide to Census data
Because the Census data files are so large, I've provided scripts to install the packages outside of the main CompJournoStick scripts.

1. '2010-census-data.bash' installs the county, Census Designate Place (CDP), tract and block group data for the 2010 Census. While these files are large, they will fit on a FAT32 disk partition.
1. '2010-census-data-block.bash' installs the block data for the 2010 Census. This file is either enormous or humongous, depending on your bandwidth costs. It will 'not' fit on a FAT32 disk partition.
1. 'older-census-data.bash' installs the 1990 Census block group data and the 2000 Census package (UScensus2000 - not in CRAN) and its data files.
